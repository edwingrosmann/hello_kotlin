import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.time.ExperimentalTime
import kotlin.time.toKotlinDuration

@ExperimentalTime
fun main() {
    val name = "Edwin"
    val birthDateCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    birthDateCal.set(1966, 1, 23)

    val birthDate = birthDateCal.toInstant().truncatedTo(ChronoUnit.DAYS)
    val now = Instant.now(Clock.system(ZoneId.of("UTC"))).truncatedTo(ChronoUnit.DAYS)
    val age = Duration.between(birthDate, now).toKotlinDuration().inDays.toInt()

    println(now.minus(birthDate)?.duration()?.toDaysPart())

    val expression = "ABKCDEOFEDTCBAABCDLEIDCBNA"
    val filtered = expression.filter { it in arrayOf('L', 'K', 'T', 'K', 'O', 'N', 'I') }
    println(
        String.format(
            "Hello %s! Wow you're %s days old I've heart. Do you know anything about '%s', %s?",
            name,
            age,
            filtered
            , birthDate.toString()
        )
    )
}

private fun Instant.duration(): Duration? {
    return Duration.of(this.epochSecond, ChronoUnit.SECONDS)!!
}

private fun Instant.minus(now: Instant?): Instant? {
    return this.minusMillis(now?.toEpochMilli()!!)
}

